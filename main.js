require('backend/server');
require("./auth/passport-strategy");
const app = require('backend/app');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const userRoutes = require('./routes/user.routes');
const githubRoutes = require('./routes/github.routes');
const path = require('path');
const express = require('express');
const secret = process.env.USER_JWT_SECRET;


app.use(passport.initialize());

// Serve only the static files form the dist directory
app.use(express.static(__dirname + '/dist/angular-frontend'));

app.get('/*', function(req,res) {
    
res.sendFile(path.join(__dirname+'/dist/angular-frontend/index.html'));
});


app.post("/login", (req, res, next)=>{
        passport.authenticate("local", { session: false }, (err, user, info)=>{
      if (err) {
        return next(err);
      }
      if (!user) {
        return res.status(401).json({message:'unauthorized',tokenData:''});
      }
      if (user) {
        let token = jwt.sign(
          { userID: user.id,firstname: user.firstname, lastname: user.lastname, email: user.email},
          secret,
          { expiresIn: "2h"}
        );
        let expiresAt = Date.now()+(120*36000);
        return res.status(200).json({ message: "OK", tokenData: JSON.stringify({token:token,expiresAt: expiresAt})});
      }
    })(req, res, next);
  });

  app.use('/api/users', userRoutes);
  app.use('/api/github', githubRoutes)