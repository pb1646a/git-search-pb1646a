export const environment = {
  production: true,
  apiUrl: 'https://git-search-pb1646a.herokuapp.com/api/',
  baseUrl: `https://git-search-pb1646a.herokuapp.com/`
};
