import { ActivatedRoute } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { LoginService } from "./login/services/login/login.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"]
})
export class AppComponent {

}
