

import { GithubSearchComponent } from "./components/github-search/github-search.component";
import { GithubUserComponent } from "./components/github-user/github-user.component";

import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthGuard } from "../login/services/guards/auth.guard";

const routes: Routes = [
  { path: "github", component: GithubSearchComponent, canActivate: [AuthGuard] },
  { path: "github/user/:login/:score", component: GithubUserComponent, canActivate: [AuthGuard] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class GithubRoutingModule {}
