import { ActivatedRoute } from "@angular/router";
import {
  GithubService,
  GitHubUser
} from "./../../services/github/github.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { FormControl } from "@angular/forms";
import { Subscription } from "rxjs";
import { filter, debounceTime, distinctUntilChanged } from "rxjs/operators";


@Component({
  selector: "app-github-search",
  templateUrl: "./github-search.component.html",
  styleUrls: ["./github-search.component.css"]
})
export class GithubSearchComponent implements OnInit, OnDestroy {
  isLoading: boolean;
  search = new FormControl();
  users: GitHubUser[] = [];
  returnUrl;
  error:any;
  constructor(
    private _gitHubService: GithubService
  ) {}

 ngOnInit() {
 this.search.valueChanges
      .pipe(
        filter(text => text.length >= 2),
        debounceTime(400),
        distinctUntilChanged()
      )
      .subscribe(value => {
        this.isLoading = true;
        this._gitHubService.getGitHubData(value).subscribe(data => {
          this.isLoading = false;
          this.users = data;
        }, error=>{
          console.log(error);
          this.error = error;
        });
      });
  }
  ngOnDestroy(){

  }
}
