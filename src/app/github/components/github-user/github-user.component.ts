import {
  GithubService,
  GitHubUser
} from "./../../services/github/github.service";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { pipe, Subscription } from "rxjs";
import { filter, debounceTime, distinctUntilChanged } from "rxjs/operators";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-github-user",
  templateUrl: "./github-user.component.html",
  styleUrls: ["./github-user.component.css"]
})
export class GithubUserComponent implements OnInit, OnDestroy {
  isLoading: boolean;
  login;
  score;
  subscription: Subscription;
  constructor(private _route: ActivatedRoute) {}

  ngOnInit() {
    this.subscription = this._route.params.subscribe(params => {
      (this.login = params["login"]), (this.score = params["score"]);
    });
  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }
}
