import { environment } from './../../../../environments/environment';
import { GitHubUser } from "./github.service";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, throwError, of } from "rxjs";
import { map, catchError } from "rxjs/operators";

export interface GitHubUser {
  html_url: string;
  avatar_url: string;
  login: string;
  score: string;
}

@Injectable({
  providedIn: "root"
})
export class GithubService {
  apiUrl = environment.apiUrl;
  constructor(private _http: HttpClient) {}
  getGitHubData(_searchTerm) {
    const data ={q: _searchTerm};
    return this._http
      .post<{ items: GitHubUser[] }>(
        `${this.apiUrl}github/search/`,
       data
      )
      .pipe(
        map(response => {
          return response.items;
        }), 
        catchError(error => {
          console.log(error);
          return throwError(error);
        })
      )
  }

}
