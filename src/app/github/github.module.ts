import { GithubUserComponent } from "./components/github-user/github-user.component";

import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { ReactiveFormsModule } from "@angular/forms";
import { RouterModule } from "@angular/router";
import { GithubSearchComponent } from "./components/github-search/github-search.component";



@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    HttpClientModule,
    RouterModule
  ],
  declarations: [GithubSearchComponent, GithubUserComponent],
  exports: [],
  providers: []
})
export class GitHubModule {}
