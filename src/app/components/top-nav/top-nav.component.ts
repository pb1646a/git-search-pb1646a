import { ActivatedRoute } from "@angular/router";
import { Component, OnInit, OnDestroy } from "@angular/core";
import { LoginService } from "../../login/services/login/login.service";
import { Router } from "@angular/router";
import { Subscription } from "rxjs";

@Component({
  selector: "app-top-nav",
  templateUrl: "./top-nav.component.html",
  styleUrls: ["./top-nav.component.css"]
})
export class TopNavComponent implements OnInit, OnDestroy {
  constructor(
    private _loginService: LoginService,
    private router: Router,
    private route: ActivatedRoute
  ) {}

  isLoggedIn;
  $isLoggedIn: Subscription;

  ngOnInit() {
    this._loginService.checkStatus();
    this._loginService.returnLoginObservable().subscribe(status => {
      if (status) {
        this.isLoggedIn = true;
      } else {
        this.isLoggedIn = false;
      }
    });
  }

  onLogout() {
    this.isLoggedIn = this._loginService.removeToken();
    this.router.navigateByUrl("/");
  }
  ngOnDestroy() {}
}
