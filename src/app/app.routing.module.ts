import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthInterceptor } from './login/services/guards/auth.interceptor';
import { RegistrationFormComponent } from "./user/registration-form/registration-form.component";
import { LoginFormComponent } from "./login/components/login-form/login-form.component";
import { HomeComponent } from "./components/home/home.component";
import { NotFoundComponent } from "./components/not-found/not-found.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

const routes: Routes = [
  { path: "", component: HomeComponent },
    {path: "login",
    component: LoginFormComponent
  },

  { path: "**", component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class RoutingModule {}
