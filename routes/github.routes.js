const express = require('express');
const router = express.Router();
const qs = require('querystring');
const request = require('request');
const rp = require('request-promise');



router.post('/search/', (req,res)=>{
    let query = qs.stringify(req.body);
    rp({url:`https://api.github.com/search/users?${query}`, method:'get', headers:{'User-Agent':'request'}, json:true}).then(result=>{
      
        return res.status(200).json({message:'ok',items: result.items});
    }).catch(error=>{
        return res.status(401).json({message:'unauthorized'});
    })
    
    

});


module.exports = router;